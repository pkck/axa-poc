# axa-poc

Axa Api integration with Bank

# Scenario
  
  * Register Customer with Axa / ING
  * Get Quote for road side service order
  * create raod side service order
 
# Tech
 
  * Spring Boot
  * Java 8 / Groovy
  * Lombok
  * Jackson
  * Intelli j idea
  
# RUN
 * just clone the repo into local computer
 * run -> mvn spring-boot:run @ root pom
 * Import Postman collection added in repo to your postman and run the services
  
# APIs

Customer Registration ( http://localhost:8081/axa-poc//user/v1/register)
=======================================================================
 
```json
## Request

{
  "invitation_code": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
  "username": "johndoey11",
  "first_name": "john",
  "last_name": "doe",
  "password": "complexEnough!1",
  "email": "john1@doe.com",
  "confirmation_language": "en",
  "confirmation_template": "greatPartner_registrationTemplate"
}


## Response 

{
  "_links": {
    "self": {
      "href": "/user/v1/identities/register"
    }
  },
  "id": "f34a7fed-55de-4847-989c-81a9caee0d09",
  "username": "johndoey",
  "first_name": "john",
  "last_name": "doe",
  "email": "john@doe.com",
  "confirmation_id": "fcece6ae-b636-af82-a748-03ad4672b7c3"
}

```

Quote Searh (http://localhost:8081/axa-poc/roadside/quotes/search)
=================================================================

```json
## Request

{
  "service": "TOWING",
  "place_type": "BASEMENT",
  "current_location": {
    "longitude": 0,
    "latitude": 0
  },
  "destination_location": {
    "longitude": 0,
    "latitude": 0
  },
  "vehicle": {
    "brand": "Audi",
    "model": "A5"
  },
  "to_schedule_on": "2010-01-01T12:00:00+01:00"
}



## Response 

[
  {
    "quote_id": "1",
    "provider": {
      "provider_id": "1234",
      "url": "https://localhost:3007/service/vexp/roadside_providers/1234"
    },
    "technician_location": {
      "longitude": 164.754167,
      "latitude": -77.508333
    },
    "distance": {
      "value": "45",
      "unit": "KMET",
      "is_as_crow_flies": false
    },
    "price": {
      "value": "250",
      "currency": "EUR"
    }
  },
  {
    "quote_id": "8",
    "price": {
      "value": "150",
      "currency": "EUR"
    }
  },
  {
    "quote_id": "10",
    "provider": {
      "provider_id": "12345",
      "url": "https://localhost:3007/service/vexp/roadside_providers/12345"
    },
    "technician_location": {
      "longitude": 162.755167,
      "latitude": -77.508333
    },
    "distance": {
      "value": "55",
      "unit": "KMET",
      "is_as_crow_flies": false
    },
    "price": {
      "value": "240",
      "currency": "EUR"
    }
  },
  {
    "quote_id": "15",
    "provider": {
      "provider_id": "12346",
      "url": "https://localhost:3007/service/vexp/roadside_providers/12346"
    },
    "technician_location": {
      "longitude": 161.75516,
      "latitude": -77.508033
    },
    "distance": {
      "value": "65",
      "unit": "KMET",
      "is_as_crow_flies": true
    },
    "price": {
      "value": "220",
      "currency": "EUR"
    }
  }
]

```

Create Service order (http://localhost:8081/axa-poc/roadside/orders)
==================================================================

## Request

```json
[
{
  "service": "TOWING",
  "place_type": "BASEMENT",
  "current_location": {
    "longitude": 0,
    "latitude": 0
  },
  "destination_location": {
    "longitude": 0,
    "latitude": 0
  },
  "vehicle": {
    "brand": "Audi",
    "model": "A5"
  },
  "to_schedule_on": "2010-01-01T12:00:00+01:00"
}



## Response 


  {
    "quote_id": "1",
    "provider": {
      "provider_id": "1234",
      "url": "https://localhost:3007/service/vexp/roadside_providers/1234"
    },
    "technician_location": {
      "longitude": 164.754167,
      "latitude": -77.508333
    },
    "distance": {
      "value": "45",
      "unit": "KMET",
      "is_as_crow_flies": false
    },
    "price": {
      "value": "250",
      "currency": "EUR"
    }
  },
  {
    "quote_id": "8",
    "price": {
      "value": "150",
      "currency": "EUR"
    }
  },
  {
    "quote_id": "10",
    "provider": {
      "provider_id": "12345",
      "url": "https://localhost:3007/service/vexp/roadside_providers/12345"
    },
    "technician_location": {
      "longitude": 162.755167,
      "latitude": -77.508333
    },
    "distance": {
      "value": "55",
      "unit": "KMET",
      "is_as_crow_flies": false
    },
    "price": {
      "value": "240",
      "currency": "EUR"
    }
  },
  {
    "quote_id": "15",
    "provider": {
      "provider_id": "12346",
      "url": "https://localhost:3007/service/vexp/roadside_providers/12346"
    },
    "technician_location": {
      "longitude": 161.75516,
      "latitude": -77.508033
    },
    "distance": {
      "value": "65",
      "unit": "KMET",
      "is_as_crow_flies": true
    },
    "price": {
      "value": "220",
      "currency": "EUR"
    }
  }
]
```

