package com.axa.poc.axapoc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class AxapocApplication {

	static void main(String[] args) {
		SpringApplication.run AxapocApplication, args
	}
}
