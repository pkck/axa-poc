package com.axa.poc.axapoc.controller
import com.axa.poc.axapoc.Service.RegisterService
import com.axa.poc.axapoc.model.RegisterRequest
import com.axa.poc.axapoc.model.RegisterResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException

import javax.validation.Valid


@RequestMapping("/axa-poc")
@RestController
class CustomerController {

   @Autowired RegisterService registerService

    @PostMapping(value = "/user/v1/register")
    ResponseEntity<?> getService(@RequestBody @Valid RegisterRequest registerRequest) throws Exception,HttpClientErrorException {
        RegisterResponse registerResponse = new RegisterResponse()
        registerResponse = registerService.createUser(registerRequest)
        return new ResponseEntity<>(registerResponse, HttpStatus.OK)
    }
}
