package com.axa.poc.axapoc.controller

import com.axa.poc.axapoc.Service.QuoteSearchService
import com.axa.poc.axapoc.Service.RoadSideOrderService
import com.axa.poc.axapoc.model.QuoteSearchRequest
import com.axa.poc.axapoc.model.QuoteSearchResponse
import com.axa.poc.axapoc.model.RegisterRequest
import com.axa.poc.axapoc.model.RegisterResponse
import com.axa.poc.axapoc.model.RoadSideOrderRequest
import com.axa.poc.axapoc.model.RoadSideOrderResponse
import org.springframework.beans.factory.annotation.Autowire
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.HttpClientErrorException

import javax.validation.Valid


@RequestMapping("/axa-poc")
@RestController
class RoadSideServiceController {

    @Autowired QuoteSearchService quoteSearchService

    @Autowired RoadSideOrderService roadSideOrderService

    @PostMapping(value = "/roadside/quotes/search")
    ResponseEntity<?> getQuotes(@RequestBody @Valid QuoteSearchRequest quoteSearchRequest) throws Exception,HttpClientErrorException {
        List<QuoteSearchResponse> quoteSearchResponse = null
        quoteSearchResponse = quoteSearchService.getQuote(quoteSearchRequest)
        return new ResponseEntity<>(quoteSearchResponse, HttpStatus.OK)
    }


    @PostMapping(value = "/roadside/orders")
    ResponseEntity<?> createOrder(@RequestBody @Valid RoadSideOrderRequest roadSideOrderRequest) throws Exception,HttpClientErrorException {
        RoadSideOrderResponse roadSideOrderResponse = null
        roadSideOrderResponse = roadSideOrderService.createOrder(roadSideOrderRequest)
        return new ResponseEntity<>(roadSideOrderResponse, HttpStatus.OK)
    }
}

