package com.axa.poc.axapoc.util
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletResponseWrapper

class ResponseWrapper extends HttpServletResponseWrapper {
    private final ByteArrayOutputStream bos = new ByteArrayOutputStream()
    private PrintWriter writer = new PrintWriter(bos)
    private long id

    ResponseWrapper(Long requestId, HttpServletResponse response) {
        super(response)
        this.id = requestId
    }

    @Override
    ServletResponse getResponse() {
        return this
    }

    @Override
     PrintWriter getWriter() throws IOException {
        return new TeePrintWriter(super.getWriter(), writer)
    }
     byte[] toByteArray() {
        return bos.toByteArray()
    }
     long getId() {
        return id
    }
     void setId(long id) {
        this.id = id
    }



    private static class TeePrintWriter extends PrintWriter {
        PrintWriter branch
         TeePrintWriter(PrintWriter main, PrintWriter branch) {
            super(main, true);
            this.branch = branch;
        }
         void write(char[] buf, int off, int len) {
            super.write(buf, off, len)
            super.flush()
            branch.write(buf, off, len)
            branch.flush()
        }
         void write(String s, int off, int len) {
            super.write(s, off, len)
            super.flush()
            branch.write(s, off, len)
            branch.flush()
        }
         void write(int c) {
            super.write(c)
            super.flush()
            branch.write(c)
            branch.flush()
        }
         void flush() {
            super.flush()
            branch.flush()
        }
    }
}
