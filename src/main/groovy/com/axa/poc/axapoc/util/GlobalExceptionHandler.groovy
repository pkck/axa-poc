package com.axa.poc.axapoc.util

import com.axa.poc.axapoc.model.ErrorResponse
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import groovy.json.JsonSlurper
import org.springframework.web.client.HttpClientErrorException
import javax.servlet.http.HttpServletRequest


@ControllerAdvice
@Slf4j
class GlobalExceptionHandler {

    @Autowired private ObjectMapper objectMapper

    @ExceptionHandler(value = HttpClientErrorException.class)
    @ResponseBody
    ResponseEntity<?> handleHttpClientErrorException(HttpServletRequest request, HttpClientErrorException ex) {
        log.info("ExceptionAdvice webRequest:", ex)
        def errorResponse = new JsonSlurper().parseText(ex.getResponseBodyAsString())
        ErrorResponse errorResponse1 = new ErrorResponse()
        errorResponse1.error = errorResponse.'error'
        errorResponse1.statusCode = errorResponse.'status_code'
        errorResponse1.description = errorResponse.'error_description'
        new ResponseEntity<>(errorResponse1, HttpStatus.BAD_REQUEST)
    }



    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    ResponseEntity<?> exceptionHandler(HttpServletRequest request, Exception ex) {
        log.info("ExceptionAdvice webRequest:", ex)
        ErrorResponse errorResponse = new ErrorResponse()
        errorResponse.statusCode = 500
        errorResponse.description = ex.message
        new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR)
    }
}

