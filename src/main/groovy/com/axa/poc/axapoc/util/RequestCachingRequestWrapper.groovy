package com.axa.poc.axapoc.util

import javax.servlet.ReadListener
import javax.servlet.ServletInputStream
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper
import org.springframework.web.util.WebUtils

class RequestCachingRequestWrapper extends HttpServletRequestWrapper{

        private final ByteArrayOutputStream bos = new ByteArrayOutputStream()
        private final ServletInputStream inputStream

        private long id
        private BufferedReader reader

       long getId() {
            return id
        }

         void setId(long id) {
            this.id = id
        }

    RequestCachingRequestWrapper(Long requestId,HttpServletRequest request) throws IOException {
            super(request)
            this.inputStream = new RequestCachingInputStream(request.getInputStream())
            this.id=requestId
        }


        @Override
         ServletInputStream getInputStream() throws IOException {
            return inputStream
        }
        @Override
         String getCharacterEncoding() {
            return super.getCharacterEncoding() != null ? super.getCharacterEncoding() :
                    WebUtils.DEFAULT_CHARACTER_ENCODING
        }
        @Override
         BufferedReader getReader() throws IOException {
            if (this.reader == null) {
                this.reader = new BufferedReader(new InputStreamReader(inputStream, getCharacterEncoding()))
            }
            return this.reader
        }

    byte[] toByteArray() {
            return this.bos.toByteArray()
        }


        private class RequestCachingInputStream extends ServletInputStream {
            private final ServletInputStream is;
            private RequestCachingInputStream(ServletInputStream is) {
                this.is = is
            }
            @Override
             int read() throws IOException {
                int ch = is.read()
                if (ch != -1) {
                    bos.write(ch)
                }
                return ch
            }
            @Override
            boolean isFinished() {
                // TODO Auto-generated method stub
                return false
            }
            @Override
             boolean isReady() {
                // TODO Auto-generated method stub
                return false
            }
            @Override
             void setReadListener(ReadListener arg0) {
                // TODO Auto-generated method stub

            }
        }
    }

