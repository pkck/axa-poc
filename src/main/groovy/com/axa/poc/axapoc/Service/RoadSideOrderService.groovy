package com.axa.poc.axapoc.Service

import com.axa.poc.axapoc.model.RoadSideOrderRequest
import com.axa.poc.axapoc.model.RoadSideOrderResponse
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

@Slf4j
@Service
class RoadSideOrderService {

    @Value('${axa.sandbox.host}')
    String host

    @Value('${axa.quote.roadSideOrderUri}')
    String roadSideSerrviceOrderUri


    @Autowired ObjectMapper objectMapper

    @Autowired RestTemplate restTemplate
    RoadSideOrderResponse createOrder(RoadSideOrderRequest roadSideOrderRequest){

        ResponseEntity<RoadSideOrderResponse> roadSideOrderResponse =null
        try {
            HttpEntity requestEntity = new HttpEntity(roadSideOrderRequest)
            roadSideOrderResponse = restTemplate.exchange("$host$roadSideSerrviceOrderUri", HttpMethod.POST, requestEntity,RoadSideOrderResponse.class)
        }catch (HttpClientErrorException e){
            log.error("Exception occurred while calling quote search",e)
            throw e
        }catch(Exception e){
            log.error("Exception occurred while calling quote search",e)
            throw e
        }
        roadSideOrderResponse.getBody()
    }

}
