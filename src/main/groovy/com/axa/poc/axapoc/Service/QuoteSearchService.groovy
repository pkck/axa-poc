package com.axa.poc.axapoc.Service

import com.axa.poc.axapoc.model.QuoteSearchRequest
import com.axa.poc.axapoc.model.QuoteSearchResponse
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

@Service
@Slf4j
class QuoteSearchService {

    @Value('${axa.sandbox.host}')
    String host

    @Value('${axa.quote.searchUri}')
    String quoteSeaerchUri

    @Value('${axa.quote.roadSideOrderUri}')
    String roadSideSerrviceOrder


    @Autowired ObjectMapper objectMapper

    @Autowired RestTemplate restTemplate

    List<QuoteSearchResponse> getQuote(QuoteSearchRequest quoteSearchRequest){

        ResponseEntity<List<QuoteSearchResponse>> response =null
        try {
            HttpEntity requestEntity = new HttpEntity(quoteSearchRequest)
            response = restTemplate.exchange("$host$quoteSeaerchUri", HttpMethod.POST, requestEntity,new ParameterizedTypeReference<List<QuoteSearchResponse>>(){})
        }catch (HttpClientErrorException e){
            log.error("Exception occurred while calling quote search",e)
            throw e
        }catch(Exception e){
            log.error("Exception occurred while calling quote search",e)
            throw e
        }
        List<QuoteSearchResponse> quoteSearchResponseList = response.getBody()
        quoteSearchResponseList
    }


}
