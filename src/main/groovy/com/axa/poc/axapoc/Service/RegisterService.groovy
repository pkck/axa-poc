package com.axa.poc.axapoc.Service

import com.axa.poc.axapoc.model.RegisterRequest
import com.axa.poc.axapoc.model.RegisterResponse
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

@Slf4j
@Service
class RegisterService {

    @Value('${axa.sandbox.host}')
    String host

    @Value('${axa.sandbox.createUri}')
    String createUri

    @Autowired ObjectMapper objectMapper

    @Autowired RestTemplate restTemplate

    RegisterResponse createUser(RegisterRequest registerRequest){
        RegisterResponse registerResponse = null
        try {
            registerResponse = restTemplate.postForObject("$host$createUri", registerRequest,RegisterResponse.class)
        }catch (HttpClientErrorException e){
            log.error("Exception occurred while calling create user",e)
            throw e
        }catch(Exception e){
            log.error("Exception occurred while calling create user",e)
            throw e
        }
        return registerResponse
    }
}
