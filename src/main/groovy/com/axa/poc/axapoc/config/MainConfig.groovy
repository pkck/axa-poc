package com.axa.poc.axapoc.config

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.deser.std.DateDeserializers
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.DateSerializer
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.conn.ssl.TrustStrategy
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import org.apache.http.ssl.SSLContexts
import javax.net.ssl.SSLContext;
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.security.KeyManagementException
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException


@Configuration
class MainConfig {

    @Value('${axa.connectTimeOut}')
    private  int connectTimeOut

    @Value('${axa.connectionRequestTimeout}')
    private  int connectionRequestTimeOut

    @Value('${axa.readTimeout}')
    private int readTimeOut

    @Bean
    RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

        //to handle https requests
        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, new TrustStrategy() {
            @Override
            boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                return true
            }
        }).build()

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext)
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build()
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory()
        requestFactory.readTimeout=readTimeOut
        requestFactory.connectionRequestTimeout=connectionRequestTimeOut
        requestFactory.connectTimeout=connectTimeOut
        requestFactory.setHttpClient(httpClient)
        RestTemplate restTemplate = new RestTemplate(requestFactory)
        return restTemplate
    }


    @Bean
    ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper()
        objectMapper.with {
            configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false)
            configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
            configure(SerializationFeature.INDENT_OUTPUT, false)
            configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true)
            enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)
            setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        }
        objectMapper.registerModule(simpleModule())
        objectMapper
    }


    private SimpleModule simpleModule() {
        SimpleModule module = new SimpleModule()
        module.with {
            addSerializer(Date, new DateSerializer())
            addDeserializer(Date, new DateDeserializers.DateDeserializer())
        }
    }


}