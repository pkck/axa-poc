package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.Data


@Data
class Self {

    @JsonProperty("href")
    private String href;
}
