package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor
import javax.validation.constraints.NotNull

@Data
@NoArgsConstructor
@AllArgsConstructor
class QuoteSearchRequest {

    @JsonProperty("service")
    @NotNull
    private String service

    @JsonProperty("place_type")
    @NotNull
    private String placeType

    @NotNull
    @JsonProperty("current_location")
    private CurrentLocation currentLocation

    @NotNull
    @JsonProperty("destination_location")
    private DestinationLocation destinationLocation

    @JsonProperty('vehicle')
    @NotNull
    private Vehicle vehicle

    @JsonProperty("to_schedule_on")
    @NotNull
    private String scheduleOn

}
