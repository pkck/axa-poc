package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.Data


@Data
class ErrorResponse {

    @JsonProperty("code")
    private String error = null

    @JsonProperty("status_code")
    private String statusCode = null

    @JsonProperty("error_description")
    private String description = null
}
