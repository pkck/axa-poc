package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(["brand", "model"])
class Vehicle {
    @JsonProperty("brand")
    private String brand
    @JsonProperty("model")
    private String model

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>()

    @JsonProperty("brand")
    String getBrand() {
        return brand
    }

    @JsonProperty("brand")
    void setBrand(String brand) {
        this.brand = brand
    }

    @JsonProperty("model")
    String getModel() {
        return model
    }

    @JsonProperty("model")
     void setModel(String model) {
        this.model = model
    }

    @JsonAnyGetter
     Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties
    }

    @JsonAnySetter
     void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value)
    }

}

