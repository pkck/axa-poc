package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder([
    "title",
    "first_name",
    "last_name",
    "second_last_name",
    "email",
    "person_registrations"
])
public class Person {

    @JsonProperty("title")
    private String title;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("second_last_name")
    private String secondLastName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("person_registrations")
    private List<PersonRegistration> personRegistrations = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("first_name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("last_name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("second_last_name")
    public String getSecondLastName() {
        return secondLastName;
    }

    @JsonProperty("second_last_name")
    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("person_registrations")
    public List<PersonRegistration> getPersonRegistrations() {
        return personRegistrations;
    }

    @JsonProperty("person_registrations")
    public void setPersonRegistrations(List<PersonRegistration> personRegistrations) {
        this.personRegistrations = personRegistrations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}