package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

import javax.validation.constraints.NotNull


@Data
@NoArgsConstructor
@AllArgsConstructor
class Price {

    @NotNull
    @JsonProperty("value")
    private String value

    @NotNull
    @JsonProperty("currency")
    private String currency
}
