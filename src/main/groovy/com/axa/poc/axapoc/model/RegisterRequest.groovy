package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.Data
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size


@Data
class RegisterRequest {

    @NotNull
    @JsonProperty("invitation_code")
    private String invitationCode

    @NotNull
    @Size(min=8,max = 64)
    @JsonProperty("username")
    private String userName

    @NotNull
    @Size(min=2,max = 64)
    @JsonProperty("first_name")
    private String firstName

    @NotNull
    @Size(min=2,max = 64)
    @JsonProperty("last_name")
    private String lastName

    @NotNull
    @JsonProperty("password")
    private String password

    @NotNull
    @Size(min=5,max = 128)
    @JsonProperty("email")
    private String email

    @JsonProperty("confirmation_language")
    private String confirmationLanguage

    @JsonProperty("confirmation_template")
    private String confirmationTemplate

}
