package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder


import javax.validation.constraints.NotNull



@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder([
    "quote_id",
    "provider",
    "technician_location",
    "distance",
    "price"])
class QuoteSearchResponse {

    @NotNull
    @JsonProperty('quote_id')
    private String quoteId

    @NotNull
    @JsonProperty("provider")
    private Provider provider

    @NotNull
    @JsonProperty("technician_location")
    private CurrentLocation location

    @NotNull
    @JsonProperty("distance")
    private Distance distance

    @NotNull
    @JsonProperty("price")
    private Price price
}
