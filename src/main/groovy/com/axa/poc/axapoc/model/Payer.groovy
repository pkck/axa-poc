package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder([
    "person",
    "phones",
    "fiscal_address"
])
class Payer {

    @JsonProperty("person")
    private Person person;
    @JsonProperty("phones")
    private List<Phone> phones = null;
    @JsonProperty("fiscal_address")
    private FiscalAddress fiscalAddress;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("person")
    public Person getPerson() {
        return person;
    }

    @JsonProperty("person")
    public void setPerson(Person person) {
        this.person = person;
    }

    @JsonProperty("phones")
    public List<Phone> getPhones() {
        return phones;
    }

    @JsonProperty("phones")
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @JsonProperty("fiscal_address")
    public FiscalAddress getFiscalAddress() {
        return fiscalAddress;
    }

    @JsonProperty("fiscal_address")
    public void setFiscalAddress(FiscalAddress fiscalAddress) {
        this.fiscalAddress = fiscalAddress;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}