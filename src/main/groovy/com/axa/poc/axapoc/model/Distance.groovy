package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

import javax.validation.constraints.NotNull


@Data
@NoArgsConstructor
@AllArgsConstructor
class Distance {

    @NotNull
    @JsonProperty("value")
    private String value

    @JsonProperty("unit")
    @NotNull
    private String unit

    @JsonProperty("is_as_crow_flies")
    @NotNull
    private String crowFiles
}

