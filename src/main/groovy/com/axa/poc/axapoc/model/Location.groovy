package com.axa.poc.axapoc.model

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

import javax.validation.constraints.NotNull


@Data
@NoArgsConstructor
@AllArgsConstructor
class Location {

    @NotNull
    private long longitude

    @NotNull
    private long latitude
}
