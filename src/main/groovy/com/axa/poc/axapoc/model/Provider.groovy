package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor

import javax.validation.constraints.NotNull


@Data
@NoArgsConstructor
@AllArgsConstructor
class Provider {

    @JsonProperty("provider_id")
    @NotNull
    private String providerId

    @JsonProperty("url")
    @NotNull
    private String url
}
