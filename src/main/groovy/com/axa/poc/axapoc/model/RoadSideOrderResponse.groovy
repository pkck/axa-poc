package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder([
    "_links",
    "customer_id",
    "quote_id",
    "service",
    "cause",
    "place_type",
    "vehicle",
    "is_payer_the_customer",
    "payer",
    "current_location",
    "destination_location",
    "to_schedule_on",
    "order_id",
    "status"
])
class RoadSideOrderResponse {

    @JsonProperty("_links")
    private Links links;
    @JsonProperty("customer_id")
    private String customerId;
    @JsonProperty("quote_id")
    private String quoteId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("cause")
    private String cause;
    @JsonProperty("place_type")
    private String placeType;
    @JsonProperty("vehicle")
    private Vehicle vehicle;
    @JsonProperty("is_payer_the_customer")
    private Boolean isPayerTheCustomer;
    @JsonProperty("payer")
    private Payer payer;
    @JsonProperty("current_location")
    private CurrentLocation currentLocation;
    @JsonProperty("destination_location")
    private DestinationLocation destinationLocation;
    @JsonProperty("to_schedule_on")
    private String toScheduleOn;
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("status")
    private String status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("_links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Links links) {
        this.links = links;
    }

    @JsonProperty("customer_id")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("quote_id")
    public String getQuoteId() {
        return quoteId;
    }

    @JsonProperty("quote_id")
    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    @JsonProperty("service")
    public String getService() {
        return service;
    }

    @JsonProperty("service")
    public void setService(String service) {
        this.service = service;
    }

    @JsonProperty("cause")
    public String getCause() {
        return cause;
    }

    @JsonProperty("cause")
    public void setCause(String cause) {
        this.cause = cause;
    }

    @JsonProperty("place_type")
    public String getPlaceType() {
        return placeType;
    }

    @JsonProperty("place_type")
    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    @JsonProperty("vehicle")
    public Vehicle getVehicle() {
        return vehicle;
    }

    @JsonProperty("vehicle")
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @JsonProperty("is_payer_the_customer")
    public Boolean getIsPayerTheCustomer() {
        return isPayerTheCustomer;
    }

    @JsonProperty("is_payer_the_customer")
    public void setIsPayerTheCustomer(Boolean isPayerTheCustomer) {
        this.isPayerTheCustomer = isPayerTheCustomer;
    }

    @JsonProperty("payer")
    public Payer getPayer() {
        return payer;
    }

    @JsonProperty("payer")
    public void setPayer(Payer payer) {
        this.payer = payer;
    }

    @JsonProperty("current_location")
    public CurrentLocation getCurrentLocation() {
        return currentLocation;
    }

    @JsonProperty("current_location")
    public void setCurrentLocation(CurrentLocation currentLocation) {
        this.currentLocation = currentLocation;
    }

    @JsonProperty("destination_location")
    public DestinationLocation getDestinationLocation() {
        return destinationLocation;
    }

    @JsonProperty("destination_location")
    public void setDestinationLocation(DestinationLocation destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    @JsonProperty("to_schedule_on")
    public String getToScheduleOn() {
        return toScheduleOn;
    }

    @JsonProperty("to_schedule_on")
    public void setToScheduleOn(String toScheduleOn) {
        this.toScheduleOn = toScheduleOn;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

