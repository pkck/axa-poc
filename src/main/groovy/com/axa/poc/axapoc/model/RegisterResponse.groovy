package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import lombok.Data


@Data
@JsonPropertyOrder([
    "_links", "id", "username", "first_name", "last_name", "email", "confirmation_id"
])
class RegisterResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("username")
    private String username;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("confirmation_id")
    private String confirmationId;
    @JsonProperty("_links")
    private Links links;
}
