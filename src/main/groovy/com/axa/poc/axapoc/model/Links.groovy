package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonProperty
import lombok.Data

@Data
class Links {

    @JsonProperty("self")
    private Self self;
}
