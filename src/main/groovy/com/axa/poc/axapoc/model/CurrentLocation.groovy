package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder(["longitude", "latitude"])
    class CurrentLocation {

        @JsonProperty("longitude")
        private Integer longitude

        @JsonProperty("latitude")
        private Integer latitude

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>()

        @JsonProperty("longitude")
        Integer getLongitude() {
            return longitude
        }

        @JsonProperty("longitude")
        void setLongitude(Integer longitude) {
            this.longitude = longitude
        }

        @JsonProperty("latitude")
        Integer getLatitude() {
            return latitude
        }

        @JsonProperty("latitude")
        void setLatitude(Integer latitude) {
            this.latitude = latitude
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties
        }

        @JsonAnySetter
        void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value)
        }
    }

