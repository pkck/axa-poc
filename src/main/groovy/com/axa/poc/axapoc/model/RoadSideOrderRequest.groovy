package com.axa.poc.axapoc.model

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder([
    "customer_id",
    "quote_id",
    "service",
    "cause",
    "place_type",
    "vehicle",
    "is_payer_the_customer",
    "payer",
    "current_location",
    "destination_location",
    "to_schedule_on"
])
class RoadSideOrderRequest {

    @JsonProperty("customer_id")
    private String customerId
    @JsonProperty("quote_id")
    private String quoteId
    @JsonProperty("service")
    private String service
    @JsonProperty("cause")
    private String cause
    @JsonProperty("place_type")
    private String placeType
    @JsonProperty("vehicle")
    private Vehicle vehicle
    @JsonProperty("is_payer_the_customer")
    private Boolean isPayerTheCustomer
    @JsonProperty("payer")
    private Payer payer
    @JsonProperty("current_location")
    private CurrentLocation currentLocation
    @JsonProperty("destination_location")
    private DestinationLocation destinationLocation
    @JsonProperty("to_schedule_on")
    private String toScheduleOn
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>()

    @JsonProperty("customer_id")
     String getCustomerId() {
        return customerId
    }

    @JsonProperty("customer_id")
     void setCustomerId(String customerId) {
        this.customerId = customerId
    }

    @JsonProperty("quote_id")
    String getQuoteId() {
        return quoteId
    }

    @JsonProperty("quote_id")
    void setQuoteId(String quoteId) {
        this.quoteId = quoteId
    }

    @JsonProperty("service")
    String getService() {
        return service
    }

    @JsonProperty("service")
     void setService(String service) {
        this.service = service
    }

    @JsonProperty("cause")
     String getCause() {
        return cause
    }

    @JsonProperty("cause")
     void setCause(String cause) {
        this.cause = cause
    }

    @JsonProperty("place_type")
     String getPlaceType() {
        return placeType
    }

    @JsonProperty("place_type")
     void setPlaceType(String placeType) {
        this.placeType = placeType
    }

    @JsonProperty("vehicle")
     Vehicle getVehicle() {
        return vehicle
    }

    @JsonProperty("vehicle")
     void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle
    }

    @JsonProperty("is_payer_the_customer")
     Boolean getIsPayerTheCustomer() {
        return isPayerTheCustomer
    }

    @JsonProperty("is_payer_the_customer")
     void setIsPayerTheCustomer(Boolean isPayerTheCustomer) {
        this.isPayerTheCustomer = isPayerTheCustomer
    }

    @JsonProperty("payer")
     Payer getPayer() {
        return payer
    }

    @JsonProperty("payer")
     void setPayer(Payer payer) {
        this.payer = payer
    }

    @JsonProperty("current_location")
     CurrentLocation getCurrentLocation() {
        return currentLocation
    }

    @JsonProperty("current_location")
     void setCurrentLocation(CurrentLocation currentLocation) {
        this.currentLocation = currentLocation
    }

    @JsonProperty("destination_location")
     DestinationLocation getDestinationLocation() {
        return destinationLocation
    }

    @JsonProperty("destination_location")
     void setDestinationLocation(DestinationLocation destinationLocation) {
        this.destinationLocation = destinationLocation
    }

    @JsonProperty("to_schedule_on")
     String getToScheduleOn() {
        return toScheduleOn
    }

    @JsonProperty("to_schedule_on")
     void setToScheduleOn(String toScheduleOn) {
        this.toScheduleOn = toScheduleOn
    }

    @JsonAnyGetter
     Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties
    }

    @JsonAnySetter
     void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value)
    }

}
