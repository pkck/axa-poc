package com.axa.poc.axapoc

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class AxapocApplicationTests {

	@Test
	void contextLoads() {
	}

}
